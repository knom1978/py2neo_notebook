# py2neo + neo4j + jupyter notebook 

#### Introudction

為方便使用graph database neo4j，將neo4j與python jupyter notebook整合成docker-compose solution。

#### 使用方法

* 安裝docker與docker-compose
* git clone此專案後進入目錄後打以下指令
```
sudo docker-compose up
```
* 同目錄會出現的兩個資料夾，db即是資料庫，而work則為連結container與host的folder。
* 若要stop直接ctrl+c即可

#### 故障排除
* 若jupyter notebook的work folder無法存檔，原因為work folder的權限不足。請用chmod進行修改。
